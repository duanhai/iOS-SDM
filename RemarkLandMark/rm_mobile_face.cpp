//
//  rm_mobile_face.cpp
//  FaceLandMark
//
//  Created by TJia on 2017/8/12.
//  Copyright © 2017年 XiangChuanPing. All rights reserved.
//

#include <stdio.h>
#include "rm_mobile_face.h"
#include "rm_mobile_common.h"
#include "ldmarkmodel.h"

#define MAX_FACE_COUNT 32
//#define SHOW_TEST

typedef struct
{
    unsigned short Hand_ID;
    int face_count;
    int image_width;
    int image_height;
    int image_stride;
    rm_pixel_format pixel_format;
    rm_rotate_type orientation;
    rm_mobile_face_action_t *face_action_array;
    ldmarkmodel * pFaceLand;
    cv::Mat *imgRgb24;
    cv::Mat *imgGray;
}RM_Mobile_Handle;

static unsigned short handid = 0;

/// @brief 创建实时人脸106关键点跟踪句柄
/// @param[in] model_path 模型文件的绝对路径或相对路径,例如models/track.tar
/// @param[in] config 配置选项 例如RM_MOBILE_TRACKING_DEFAULT_CONFIG，默认使用双线程跟踪，实时视频预览建议使用该配置。
//     使用单线程算法建议选择（RM_MOBILE_TRACKING_SINGLE_THREAD)
/// @parma[out] handle 人脸跟踪句柄，失败返回NULL
/// @return 成功返回RM_OK, 失败返回其他错误码,错误码定义在rm_mobile_common.h中，如RM_E_FAIL等
RM_SDK_API rm_result_t
rm_mobile_tracker_106_create(const char *model_path,
                             unsigned int config,
                             rm_handle_t *handle)
{
    RM_Mobile_Handle *pHandle = (RM_Mobile_Handle *) malloc(sizeof(RM_Mobile_Handle));
    if(pHandle == NULL)
    {
        return RM_E_OUTOFMEMORY;
    }
    
    pHandle->Hand_ID = ++handid;
    pHandle->face_count = 0;
    pHandle->face_action_array = (rm_mobile_face_action_t *)malloc(sizeof(rm_mobile_face_action_t) * MAX_FACE_COUNT);
    pHandle->pFaceLand = new ldmarkmodel(model_path);
    bool bret = load_ldmarkmodel(model_path, *(pHandle->pFaceLand));
    if (!bret)
    {
        std::cout << "open model file failed." << std::endl;
        return RM_E_FILE_NOT_FOUND;
    }
    
    pHandle->image_width = 640;
    pHandle->image_height = 480;
    pHandle->image_stride = 0;
    pHandle->pixel_format = RM_PIX_FMT_BGR888;
    pHandle->orientation = RM_CLOCKWISE_ROTATE_0;
    pHandle->imgRgb24 = new cv::Mat(pHandle->image_height, pHandle->image_width, CV_8UC3);
    pHandle->imgGray = new cv::Mat(pHandle->image_height, pHandle->image_width, CV_8UC1);

    *handle = (void *)pHandle;
    return RM_OK;
}

void Yuv420ToRgb24(IplImage *pYUV420,IplImage * pRgb24)
{
    IplImage * pYUV444 = cvCreateImage(cvGetSize(pRgb24),8,3);

    IplImage * pY = cvCreateImage(cvGetSize(pRgb24),8,1);
    IplImage * pU = cvCreateImage(cvGetSize(pRgb24),8,1);
    IplImage * pV= cvCreateImage(cvGetSize(pRgb24),8,1);

    IplImage * pU4 = cvCreateImage(cvSize(pRgb24->width/2,pRgb24->height/2),8,1);
    IplImage * pV4= cvCreateImage(cvSize(pRgb24->width/2,pRgb24->height/2),8,1);

    CvSize size = cvGetSize(pRgb24);

    memcpy(pY->imageData,pYUV420->imageData,sizeof(char)*size.width * size.height);
    memcpy(pU4->imageData,pYUV420->imageData +size.width * size.height ,sizeof(char)*size.width/2 * size.height/2);
    memcpy(pV4->imageData,pYUV420->imageData + size.width * size.height +  size.width * size.height / 4,sizeof(char)*size.width/2*size.height/2);

    cvResize(pU4,pU);
    cvResize(pV4,pV);

    cvMerge(pY,pU,pV,NULL,pYUV444);
    cvCvtColor(pYUV444,pRgb24,CV_YUV2BGR);

    cvReleaseImage(&pYUV444);
    cvReleaseImage(&pY);
    cvReleaseImage(&pU);
    cvReleaseImage(&pV);
    cvReleaseImage(&pU4);
    cvReleaseImage(&pV4);
}

/// @brief 对连续视频帧进行实时快速人脸106关键点跟踪，并检测脸部动作
/// @param handle 已初始化的实时人脸跟踪句柄
/// @param image 用于检测的图像数据
/// @param pixel_format 用于检测的图像数据的像素格式,都支持，不推荐BGRA和BGR，会慢
/// @param image_width 用于检测的图像的宽度(以像素为单位)
/// @param image_height 用于检测的图像的高度(以像素为单位)
/// @param[in] image_stride 用于检测的图像的跨度(以像素为单位)，即每行的字节数；目前仅支持字节对齐的padding，不支持roi
/// @param orientation 视频中人脸的方向
/// @param p_face_action_array 检测到的人脸106点信息和脸部动作的数组，api负责管理内存，会覆盖上一次调用获取到的数据
/// @param p_faces_count 检测到的人脸数量
/// @return 成功返回RM_OK，失败返回其他错误码,错误码定义在rm_mobile_common.h中，如ST_E_FAIL等
RM_SDK_API rm_result_t
rm_mobile_tracker_106_track_face_action(rm_handle_t handle,
                                        const unsigned char *image,
                                        rm_pixel_format pixel_format,
                                        int image_width,
                                        int image_height,
                                        int image_stride,
                                        rm_rotate_type orientation,
                                        rm_mobile_face_action_t **p_face_action_array,
                                        int *p_faces_count)
{
#ifdef TIME_TEST
    double tt_begin, tt_end;
#endif
    TIME_START()
    RM_Mobile_Handle * pHandle = (RM_Mobile_Handle *)handle;
    
    if(pixel_format == RM_PIX_FMT_GRAY8
    || pixel_format == RM_PIX_FMT_BGRA8888
    || pixel_format == RM_PIX_FMT_RGBA8888)
    {
        return RM_E_INVALID_PIXEL_FORMAT;
    }

    if (pHandle->image_width != image_width || pHandle->image_height != image_height)
    {
        if (!pHandle->imgRgb24->empty())
        {
            //pHandle->imgRgb24->release();
            delete pHandle->imgRgb24;
            printf("release imgRgb24\n");
        }
        
        if (!pHandle->imgGray->empty())
        {
            //pHandle->imgGray->release();
            delete pHandle->imgGray;
            printf("release imgGray\n");
        }
        
        pHandle->imgGray = new cv::Mat(image_height, image_width, CV_8UC1);
        pHandle->imgRgb24 = new cv::Mat(image_height, image_width, CV_8UC3);
        
        pHandle->image_width = image_width;
        pHandle->image_height = image_height;
    }
    
    if(pixel_format == RM_PIX_FMT_YUV420P ||
       pixel_format == RM_PIX_FMT_NV12 ||
       pixel_format == RM_PIX_FMT_NV21)
    {
        memcpy(pHandle->imgGray->data, image, sizeof(unsigned char) * image_width * image_height);
    }
    else if (pixel_format == RM_PIX_FMT_BGR888)
    {
        memcpy(pHandle->imgRgb24->data, image, sizeof(unsigned char)*image_width * image_height * 3);
        cv::cvtColor(*(pHandle->imgRgb24), *(pHandle->imgGray), CV_BGR2GRAY);
    }
    
    cv::Mat img_flip, img_roted;
    switch (orientation)
    {
        case RM_CLOCKWISE_ROTATE_90:
            cv::transpose(*(pHandle->imgGray), img_roted);
            break;
        case RM_CLOCKWISE_ROTATE_180:
            cv::flip(*(pHandle->imgGray), img_roted, 1);
            break;
        case RM_CLOCKWISE_ROTATE_270:
            cv::transpose(*(pHandle->imgGray), img_flip);
            cv::flip(img_flip, img_roted, -1);
            break;
        default:
            pHandle->imgGray->copyTo(img_roted);
            break;
    }
    TIME_END("Image Convert")
    
#ifdef SHOW_TEST
    cv::Mat imgtmp1, imgtmp2, imgtmp3;
    cv::resize(*(pHandle->imgGray), imgtmp1, cv::Size(300, 300));
    cv::imshow("tmp1", imgtmp1);
    if (!img_flip.empty())
    {
        cv::resize(img_flip, imgtmp2, cv::Size(300, 300));
        cv::imshow("tmp2", imgtmp2);
    }
    cv::resize(img_roted, imgtmp3, cv::Size(300, 300));
    cv::imshow("tmp3", imgtmp3);
    cv::waitKey(1);
#endif
    
    TIME_START()
    std::vector<Landmark_T> landmerks;
    pHandle->pFaceLand->track(img_roted, landmerks, false);
    TIME_END("Landmark Track")
    
    TIME_START()
    pHandle->face_count = (int)landmerks.size();
    *p_faces_count = pHandle->face_count;
    
    //memset(pHandle->face_action_array, 0, sizeof(rm_mobile_face_action_t) * MAX_FACE_COUNT);
    
    for(int i = 0; i < std::min((int)landmerks.size(), MAX_FACE_COUNT); i++)
    {
        Landmark_T temp = landmerks[i];
        
        rm_mobile_face_action_t face_action_temp;
    
        face_action_temp.face_action    = 0x00000000;
        face_action_temp.face.ID        = 0;
        face_action_temp.face.eye_dist  = 0;
        face_action_temp.face.pitch     = temp.pitch;
        face_action_temp.face.roll      = temp.roll;
        face_action_temp.face.yaw       = temp.yaw;
        face_action_temp.face.score     = temp.score;
    
        face_action_temp.face.rect.left     = temp.FaceR.x;
        face_action_temp.face.rect.top      = temp.FaceR.y;
        face_action_temp.face.rect.right    = temp.FaceR.x + temp.FaceR.width;
        face_action_temp.face.rect.bottom   = temp.FaceR.y + temp.FaceR.height;
    
        for(int j = 0; j < 106; j ++)
        {
            face_action_temp.face.points_array[j].x = temp.FacePts.at<float>(j);
            face_action_temp.face.points_array[j].y = temp.FacePts.at<float>(j+106);
        }
    
        pHandle->face_action_array[i] = face_action_temp;
    }
    //TIME_END("Landmark Track")
    
    *p_face_action_array = pHandle->face_action_array;
   
    return RM_OK;
}

/// @brief 销毁已初始化的track106句柄
/// @param[in] handle 已初始化的人脸跟踪句柄
RM_SDK_API void rm_mobile_tracker_106_destroy(rm_handle_t handle)
{
    RM_Mobile_Handle * pHandle = (RM_Mobile_Handle *)handle;
    
    if (pHandle->pFaceLand != NULL)
    {
        delete pHandle->pFaceLand;
        pHandle->pFaceLand = NULL;
    }
    
    if (pHandle->face_action_array != NULL)
    {
        free(pHandle->face_action_array);
        pHandle->face_action_array = NULL;
    }
    
    if (!pHandle->imgRgb24->empty())
    {
        //pHandle->imgRgb24->release();
        delete pHandle->imgRgb24;
        pHandle->imgRgb24 = NULL;
    }
    
    if (!pHandle->imgGray->empty())
    {
        //pHandle->imgGray->release();
        delete pHandle->imgGray;
        pHandle->imgGray = NULL;
    }
    
    free(pHandle);
    pHandle = NULL;
}


