//
//  ViewController.m
//  RemarkLandMark
//
//  Created by duanhai on 2017/8/14.
//  Copyright © 2017年 duanhai. All rights reserved.
//

#import "ViewController.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/cap_ios.h>

#include <opencv2/objdetect/objdetect.hpp>
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "rm_mobile_face.h"
#import "CanvasView.h"
//#include "TestLog.h"
using namespace std;
using namespace cv;
#define TICK(time) NSDate *time = [NSDate date]
#define TOCK(time) NSLog(@"%s: %f", #time, -[time timeIntervalSinceNow] * 1000)
#define DocumentDir [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define BundlePath(res) [[NSBundle mainBundle] pathForResource:res ofType:nil]
#define DocumentPath(res) [DocumentDir stringByAppendingPathComponent:res]


@interface ViewController ()<AVCaptureVideoDataOutputSampleBufferDelegate,CvVideoCameraDelegate>
{

    int faces_count;
    cv::Mat current_shape;
    rm_handle_t handle;
    rm_mobile_face_action_t *p_face_action_array;
    CGFloat _imageOnPreviewScale;
    CGFloat _previewImageWidth;
    CGFloat _previewImageHeight;
}

@property (nonatomic, strong) CvVideoCamera *videoCamera;
@property (nonatomic , strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic , strong) AVCaptureDevice *videoDevice;
@property (nonatomic , strong) CanvasView *viewCanvas;


@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
//    NSString *tmpDir =  NSTemporaryDirectory();
    NSString *res = [[NSBundle mainBundle] resourcePath];
    std::string modelPath = std::string([res UTF8String]);

   rm_result_t rt = rm_mobile_tracker_106_create(modelPath.c_str(), 0, &handle);

    if(rt != RM_OK)
    {
        std::cout << "create handle failed " << std::endl;
    }else {
        std::cout << "Load model succeed!" << std::endl;
    }

    [self setupAndStartCapture];
       // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - Protocol CvVideoCameraDelegate

#ifdef __cplusplus
- (void)processImage:(Mat&)image;
{
    // Do some OpenCV stuff with the image
    cv::Mat Image = image;
    NSString *tmpDir =  NSTemporaryDirectory();
    NSString *saveFileName = [tmpDir stringByAppendingString:@"/test.png"];
    std::string pngName = std::string([saveFileName UTF8String]);
    
}
#endif

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)startCapture:(id)sender {
    [self.videoCamera start];

}

- (void)hideFace {
    if (!self.viewCanvas.hidden) {
        self.viewCanvas.hidden = YES;
    }
}
- (void)showFaceLandmarksAndFaceRectWithPersonsArray:(NSMutableArray *)arrPersons
{
    if (self.viewCanvas.hidden) {
        self.viewCanvas.hidden = NO;
    }
    self.viewCanvas.arrPersons = arrPersons;
    [self.viewCanvas setNeedsDisplay];
}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {

    NSString *tmpDir =  NSTemporaryDirectory();
    NSString *pngPath = [tmpDir stringByAppendingString:@"/test.png"];
    std::string mypath =  std::string([pngPath UTF8String]);
    
    CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    
    void *baseAddress = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
    int iBytesPerRow =(int)CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0);
    int iHeight = (int)CVPixelBufferGetHeightOfPlane(pixelBuffer, 0);
    int iWidth = (int)CVPixelBufferGetWidthOfPlane(pixelBuffer, 0);
    
    size_t iTop , iBottom , iLeft , iRight;
    CVPixelBufferGetExtendedPixels(pixelBuffer, &iLeft, &iRight, &iTop, &iBottom);
    
    iWidth = iWidth + (int)iLeft + (int)iRight;
    iHeight = iHeight + (int)iTop + (int)iBottom;
//    rm_mobile_tracker_106_track
    
    TICK(xxx);
    rm_result_t rt1 = rm_mobile_tracker_106_track_face_action
    (
     handle,
     (unsigned char *)baseAddress,
     RM_PIX_FMT_NV12,
     iWidth,
     iHeight,
     iBytesPerRow,
     RM_CLOCKWISE_ROTATE_90,
     &p_face_action_array,
     &faces_count);
    TOCK(xxx);
    if (faces_count > 0) {
//        NSLog(@"oooooooooooo");
        
        NSMutableArray *arrPersons = [NSMutableArray array];
        for (int i = 0; i < faces_count; i ++) {
            
            rm_mobile_106_t stFace = p_face_action_array[i].face;
            
            NSMutableArray *arrStrPoints = [NSMutableArray array];
            CGRect rectFace = CGRectZero;
            rm_pointf_t *facialPoints = stFace.points_array;
            for(int i = 0; i < 106; i ++) {
                CGPoint point;
                if (1) {
                    point.x = _imageOnPreviewScale * facialPoints[i].x;
                    point.y = _imageOnPreviewScale * facialPoints[i].y;
                } else {
                    point.x = _previewImageWidth - _imageOnPreviewScale * facialPoints[i].y;
                    point.y = _imageOnPreviewScale* facialPoints[i].x;
                }
                [arrStrPoints addObject:[NSValue valueWithCGPoint:point]];
            }
            
            rm_rect_t rect = stFace.rect;
            
            if (1) {
                rectFace = CGRectMake(_imageOnPreviewScale * rect.left,
                                      _imageOnPreviewScale * rect.top,
                                      _imageOnPreviewScale * rect.right - _imageOnPreviewScale * rect.left,
                                      _imageOnPreviewScale * rect.bottom - _imageOnPreviewScale * rect.top);
            } else {
                rectFace = CGRectMake(_previewImageWidth - _imageOnPreviewScale * rect.top - (_imageOnPreviewScale * rect.right - _imageOnPreviewScale * rect.left) ,
                                      _imageOnPreviewScale * rect.left,
                                      _imageOnPreviewScale * rect.right - _imageOnPreviewScale * rect.left,
                                      _imageOnPreviewScale * rect.bottom - _imageOnPreviewScale * rect.top);
            }
            
            NSMutableDictionary *dicPerson = [NSMutableDictionary dictionary];
            [dicPerson setObject:arrStrPoints forKey:POINTS_KEY];
            [dicPerson setObject:[NSValue valueWithCGRect:rectFace] forKey:RECT_KEY];
            
            [arrPersons addObject:dicPerson];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self showFaceLandmarksAndFaceRectWithPersonsArray:arrPersons];
        } );
        
    }else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideFace];
        } );
    }
    
    //    t = (double)cvGetTickCount() - t;
    
    //    std::cout << "time : " << t / (cvGetTickFrequency() * 1000) << std::endl;
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    
    
    
}


- (void)setupAndStartCapture
{
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    // Set the camera preview size
    session.sessionPreset = AVCaptureSessionPreset640x480;
    CGFloat imageWidth = 480;
    CGFloat imageHeight = 640;
    
    // Get the preview frame size.
    self.captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    self.captureVideoPreviewLayer.frame = self.view.bounds;
    CGFloat previewWidth = self.captureVideoPreviewLayer.frame.size.width;
    CGFloat previewHeight = self.captureVideoPreviewLayer.frame.size.height;
    [self.captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.view.layer addSublayer:self.captureVideoPreviewLayer];
    
    // Calculate the width, height and scale rate to display the preview image
    _imageOnPreviewScale = MAX(previewHeight/imageHeight, previewWidth/imageWidth);
    _previewImageWidth = imageWidth * _imageOnPreviewScale;
    _previewImageHeight = imageHeight * _imageOnPreviewScale;
    
    self.viewCanvas = [[CanvasView alloc] initWithFrame:CGRectMake(0., 0., _previewImageWidth, _previewImageHeight)];
    self.viewCanvas.center = self.view.center;
    self.viewCanvas.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.viewCanvas];
    
    NSArray *devices = [AVCaptureDevice devices];
    for (AVCaptureDevice *device in devices) {
        
        if ([device hasMediaType:AVMediaTypeVideo]) {
            
            if ([device position] == AVCaptureDevicePositionFront) {
                
                self.videoDevice = device;
            }
        }
    }
    
    NSError *error = nil;
    AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:self.videoDevice error:&error];
    
    if (!videoInput) {
        
        NSLog(@"trying to open camera: %@",error);
        
        return;
    }
    
    AVCaptureVideoDataOutput * dataOutput = [[AVCaptureVideoDataOutput alloc] init];
    
    [dataOutput setAlwaysDiscardsLateVideoFrames:YES];
    
    [dataOutput setVideoSettings:@{(id)kCVPixelBufferPixelFormatTypeKey: @(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)}];
    dispatch_queue_t queue = dispatch_queue_create("dataOutputQueue", NULL);
    [dataOutput setSampleBufferDelegate:self queue:queue];
    
    [session beginConfiguration];
    
    if ([session canAddInput:videoInput]) {
        [session addInput:videoInput];
    }
    if ([session canAddOutput:dataOutput]) {
        [session addOutput:dataOutput];
    }
    [session commitConfiguration];
    
    [session startRunning];
}

@end
